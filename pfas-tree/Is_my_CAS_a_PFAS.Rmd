---
title: "Example Workflow: Is my CAS a PFAS?"
author: 
- "Emma L. Schymanski^1^* and Evan E. Bolton^2^*"
date: "17 July 2024"
output: pdf_document
csl: journal-of-cheminformatics.csl
bibliography: refs.bib
urlcolor: blue
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
#knitr::opts_chunk$set(fig.pos = "!H", out.extra = "")
```

^1^ Luxembourg Centre for Systems Biomedicine (LCSB), 
University of Luxembourg, 6 avenue du Swing, 4367, Belvaux, Luxembourg. 
*ELS: [emma.schymanski@uni.lu](mailto:emma.schymanski@uni.lu),
ORCID [0000-0001-6868-8145](http://orcid.org/0000-0001-6868-8145).

^2^ National Center for Biotechnology Information (NCBI), National 
Library of Medicine (NLM), National Institutes of Health (NIH), 
Bethesda, MD, 20894, USA. 
*EEB: [evan.bolton@nih.gov](mailto:evan.bolton@nih.gov),
ORCID: [0000-0002-5959-6190](http://orcid.org/0000-0002-5959-6190).


## Preamble

This is an example workflow showing how to use the
"[PFAS and Fluorinated Compounds in PubChem Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)" 
(hereafter 
"[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)")
to explore which entries in a list of Chemical Abstract Service (CAS) 
Registry Numbers meet the criteria of being a 
per- and polyfluoroalkyl substances (PFAS) and why. This document has 
been produced in light of external feedback and questions. 
Please note that a similar workflow could be performed for any list 
of inputs suitable for the PubChem 
[Identifier Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi).

More extensive documentation about the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
can be found 
[here](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/blob/main/pfas-tree/PFAS_Tree.pdf) and in Schymanski et al (2023) @schymanski_PFAS_tree_EST_2023.


## The Scenario: How many compounds in a list are PFAS?

<!-- This example is a mock-up of a user query using an open dataset that  -->
<!-- contains a mixture of PFAS, organofluorine and non-fluorine containing  -->
<!-- compounds.  -->
The list chosen for this example is the 
[S36 UBAPMT](https://doi.org/10.5281/zenodo.2653212) list @arp_s36_2022,
a list of persistent, mobile and toxic (PMT) compounds 
provided to the NORMAN Suspect List Exchange 
([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)) @mohammed_taha_norman_2022
by Hans Peter Arp (NGI, Norway) and the German Environment Agency 
(Umweltbundesamt, UBA).

A file containing all the CAS numbers is available to 
[view](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/blob/main/pfas-tree/UBAPMT_CAS.txt)
or [download](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/raw/main/pfas-tree/UBAPMT_CAS.txt?inline=false).

### The Process, Step by Step

#### 1. Identifier Exchange

First, go to the PubChem 
[Identifier Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi)
and either upload the list of CAS numbers, or copy-paste the list into the 
box (see Figure 1). 
Note that this step can be performed using other identifiers by selecting 
the respective options in the drop-down menu (see inset, Figure 1). 

![_Converting CAS numbers into Entrez History with PubChem [ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi) (11 May 2023)._](fig/IDExchange_UBAPMT_CAS.png)

#### 2. Classification Browser: PubChem PFAS Tree

Next, go to the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120).
If the tab was already open, please refresh (F5, or "Refresh" in the browser).
An option with the search result should appear (see Figure 2). Selecting 
this option will now display all entries in the UBAPMT list that are 
either PFAS or organofluorine compounds. See Figure 2 for an overview of 
this display before expanding into more detail. 

![_Viewing the PFAS/Organofluorine contents of UBAPMT in the [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) (17 July 2024)._](fig/UBAPMT_CAS_in_PFAS_Tree.png)


#### 3. View and Export the Details

The nodes of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
can be expanded to find out more details about the entries, as shown in Figure 3. 
Clicking on the blue box next to each node header will send the contents
to PubChem Search, for viewing or downloading - see Figure 4. 
To see which PubChem Compound Identifiers (CIDs) were found for each 
CAS number, adjust the ID Exchange settings as indicated in Figure 5. 

![_Detailed view of the PFAS/Organofluorine contents of UBAPMT in the [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) (17 July 2023)._](fig/UBAPMT_CAS_in_PFAS_Tree_breakdown.png)

![_Viewing and downloading entries in PubChem Search (11 May 2023)._](fig/UBAPMT_CAS_in_PFAS_Tree_view_download.png)

![_Converting CAS numbers into CIDs with PubChem [ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi) (11 May 2023)._](fig/IDExchange_UBAPMT_CAS_twocol.png)

### Closing

We hope this document provided some insights how to use the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
to explore the classification of chemicals in any given input list (here
exemplified with CAS numbers). Please note that any identifier that can 
be input into the ID Exchange will work similarly, as shown in Figure 1. 

To find out more, further documentation about the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
can be found 
[here](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/blob/pfas-docs-updates/pfas-tree/PFAS_Tree.pdf).




## Contact Details

For questions about the specific content of this document, 
please reach out to [Emma Schymanski](mailto:emma.schymanski@uni.lu).

For general questions about PubChem, please reach out to the 
[PubChem Help mailing list](mailto:pubchem-help@ncbi.nlm.nih.gov)
for further support. 


## Acknowledgements

Many thanks to the PubChem and ECI teams for their efforts, especially 
Paul Thiessen and Jeff Zhang for their efforts related to the PubChem PFAS Tree.

## References

