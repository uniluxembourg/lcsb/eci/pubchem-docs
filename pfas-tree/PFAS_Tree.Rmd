---
title: "PFAS and Fluorinated Compounds in PubChem Tree"
author: 
- "Emma L. Schymanski^1^*, Parviel Chirsir^1^, Todor Kondic^1^,"
- "Paul A. Thiessen^2^, Jian Zhang^2^ and Evan E. Bolton^2^*"
date: "Last updated by ELS, 25 January 2024"
output: pdf_document
csl: journal-of-cheminformatics.csl
bibliography: refs.bib
urlcolor: blue
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_chunk$set(warning = FALSE, message = FALSE) 
#knitr::opts_chunk$set(fig.pos = "!H", out.extra = "")
```

^1^ Luxembourg Centre for Systems Biomedicine (LCSB), 
University of Luxembourg, 6 avenue du Swing, 4367, Belvaux, Luxembourg. 
*ELS: [emma.schymanski@uni.lu](mailto:emma.schymanski@uni.lu).
ORCIDs: ELS: [0000-0001-6868-8145](http://orcid.org/0000-0001-6868-8145),
PC: [0000-0002-9932-8609](http://orcid.org/0000-0002-9932-8609),
TK: [0000-0001-6662-4375](https://orcid.org/0000-0001-6662-4375).

^2^ National Center for Biotechnology Information (NCBI), National 
Library of Medicine (NLM), National Institutes of Health (NIH), 
Bethesda, MD, 20894, USA. 
*EEB: [evan.bolton@nih.gov](mailto:evan.bolton@nih.gov).
ORCIDs: PAT: [0000-0002-1992-2086](https://orcid.org/0000-0002-1992-2086), 
JZ: [0000-0002-6192-4632](https://orcid.org/0000-0002-6192-4632), 
EEB: [0000-0002-5959-6190](http://orcid.org/0000-0002-5959-6190).


## Preamble

This document describes the "[PFAS and Fluorinated Compounds in 
PubChem Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)" 
(hereafter 
"[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)")
<!-- on the  -->
<!-- [Classification Brower](https://pubchem.ncbi.nlm.nih.gov/classification/) -->
in [PubChem](https://pubchem.ncbi.nlm.nih.gov/) [@kim_pubchem_2022], 
developed jointly between PubChem (NCBI/NLM/NIH) and the 
Environmental Cheminformatics group 
([ECI](https://wwwen.uni.lu/lcsb/research/environmental_cheminformatics))
at the [LCSB](https://wwwen.uni.lu/lcsb/), 
[University of Luxembourg](https://wwwen.uni.lu/), in consultation with 
several community representatives (see [Contributions](#contrib)
and [Acknowledgements](#ack)). 
The 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
(see [Figure 1](#treenodes) and [Contents listing](#cont))
includes all compounds in [PubChem](https://pubchem.ncbi.nlm.nih.gov/) 
satisfying various definitions, as explained later in this document.
Note that each compound in PubChem has a PubChem Compound Identifier (CID), and the
blue numbers next to each node header reflects the number of 
compounds (_i.e._ CIDs) in that node.

More details on the general 
[PubChem Classification Brower](https://pubchem.ncbi.nlm.nih.gov/classification/)
features are given in the Section [Exploring the Tree](#search), via the PubChem 
[documentation](https://pubchem.ncbi.nlm.nih.gov/docs/classification-browser) 
and [help](https://pubchem.ncbi.nlm.nih.gov/classification/docs/classification_help.html) 
pages. 
You can also read more about 
[PFAS in PubChem: 7 Million and Growing](https://doi.org/10.1021/acs.est.3c04855) 
in ES&T, DOI: [10.1021/acs.est.3c04855](https://doi.org/10.1021/acs.est.3c04855)
[@schymanski_PFAS_tree_EST_2023], or watch
two videos on the [ZeroPM](https://zeropm.eu/) YouTube channel, 
a ~23 min [interactive walkthrough](https://www.youtube.com/watch?v=g-sAazaagas)
(Jun. 2022)
and a ~1 hour [webinar](https://www.youtube.com/watch?v=jkdvCs4pGzU)
(Mar. 2023) [@schymanski_zeropm_2023].
<!-- plus a [preprint](https://doi.org/10.26434/chemrxiv-2023-j823z)  -->
<!-- on ChemRxiv [@schymanski_per_2023]. -->


## Contents {#cont}

<!-- This document is organised into several sections, as follows:  -->
Table: _Contents list for the PubChem PFAS Tree documentation._

| Section | Navigation | PDF Page |
|------------------------------|---------|:----:|
|PubChem PFAS Tree Nodes | [Go to heading](#treenodes) | 2 |
| - _OECD PFAS Definition_ | [Go to heading](#oecddef) | 2 |
| - _Organofluorine Compounds_ | [Go to heading](#orgf) | 5 |
| - _Other Diverse Fluorinated Compounds_ | [Go to heading](#divf) | 6 |
| - _PFAS and Fluorinated Compound Collections_ | [Go to heading](#lists) | 7 |
| - _Regulatory PFAS Collections_ | [Go to heading](#regcoll) | 7 |
| - _PFAS Breakdowns by Chemistry_ | [Go to heading](#chem) | 9 |
|Exploring the PubChem PFAS Tree | [Go to heading](#search) | 10 |
| - _Download via PubChem Search_ | [Go to heading](#pc-search) | 10 |
| - _PubChem Saved Searches_ | [Go to heading](#savedsearch) | 11 |
| - _Interactions via Entrez_ | [Go to heading](#entrez) | 12 |
|Extra Details | [Go to heading](#details) | 15 |
|Statements and References | [Go to heading](#statements) | 16 | 
<!-- |Statements | [Go to heading](#statements) | 14 |  -->
<!-- |References | [Go to heading](#refs) | 14 | -->



## PubChem PFAS Tree Nodes {#treenodes}

The tree is currently split into six main nodes that are constructed and 
compiled separately (see [Figure 1](#treenodes)). 
<!-- Nodes that are under development are released once they are ready.  -->
Further details about each of the nodes are given below. 
PubChem Classification Browser features are described further in the
Section [Exploring the Tree](#search).

![_The "[PFAS and Fluorinated Compounds in PubChem Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)" Landing Page (25 Jan. 2025)._](fig/PFAS_Tree_Landing.png)



### OECD PFAS Definition {#oecddef}

This node is constructed out of per- and polyfluoroalkyl substances 
(PFAS) satisfying the OECD 2021 definition (contains at least one saturated CF~2~ or 
CF~3~ part) in the 2021 OECD Report 
[ENV/CBC/MONO(2021)25](https://one.oecd.org/document/ENV/CBC/MONO(2021)25/En/pdf)
[@oecd_reconciling_2021]. 
Note that here, "**PFAS part**" is used to describe a connected portion of 
the molecule that satisfies the OECD PFAS definition. A given molecule may have
more than one PFAS part present, some examples are given in Figure 2,
along with the count of parts. 
<!-- For more information, see section "[Further Details](#details)". -->

Browsing the ~7 million entries in this node (see Figure 3) is challenging.
Since most of these PFAS contain isolated CF~2~ (>760 K entries) or 
CF~3~ groups (>6 M entries), these were separated into individual sections
(see "[Isolated CF~2~ and CF~3~ Nodes](#isonodes)").
<!-- (see [next section](#isonodes)).  -->
Approximately 229 K compounds contain PFAS parts larger than CF~2~/CF~3~
(see "[PFAS Parts Larger than CF~2~/CF~3~](#largerparts)").


![_Examples of molecules with varying PFAS parts highlighted, drawn using [CDK Depict](https://www.simolecule.com/cdkdepict/depict.html) [@mayfield_cdk]._](fig/PFAS_parts_CDK.png)


The _OECD PFAS Definition_ node,
with the top two level subnodes, is shown in Figure 3. 

![_The OECD PFAS Definition part of the PFAS tree, with top two subnodes (25 Jan. 2025)._](fig/OECDPFAS_TopTwoSubnodes.png)



### OECD PFAS - Isolated CF~2~ and CF~3~ Nodes {#isonodes} 

The _Isolated CF~2~ and CF~3~_ subnodes of the _OECD PFAS Definition_ node 
allows the browsing of all PFAS 
molecules in PubChem containing at least one isolated CF~2~ (top subnode) 
or one isolated CF~3~ (next subnode). These are broken down similarly, 
as shown in Figure 4 for CF~2~. 

![_The isolated CF~2~ section of the OECD PFAS Definition node, with breakdown of the major parts (numbers as of 25 Jan. 2025)._](fig/OECDPFAS_CF2combi.png)

The larger PFAS parts (left) are broken down by part type (linear, branched, 
_etc._). Within these subcategories, dynamic construction is used. 
If many (>20) variants are present, a breakdown by number of PFAS parts 
is added (_e.g.,_ Figure 4, middle left, "_Contains isolated cyclic 
PFAS part_"), if not, a list of the possibilities is given directly 
(_e.g.,_ Figure 4, lower left, "_Contains isolated unsaturated-branched part_").

The "_Contains only isolated CF~2~_" 
(or, for the CF~3~ node, "_Contains only isolated CF~3~_")
is broken down by the number of isolated groups (CF~2~ or, 
for the CF~3~ node, by CF~3~ groups) - see Figure 4, middle panel. In both
cases, the vast majority of molecules have only one isolated group. 
The "_Contains only isolated CF~2~/CF~3~_" node is also broken down by
the number of groups, sorted by increasing number of CF~2~ groups
(for both nodes). See Figure 4, right panel. 



### OECD PFAS - PFAS Parts Larger than CF~2~/CF~3~ {#largerparts}

The "_Molecule contains PFAS parts larger than CF~2~/CF~3~_" part of the 
OECD PFAS node includes >220 K molecules, which can be browsed 
in two major breakdowns, by _isolated PFAS part count_ (see Figure 5) 
and by _isolated PFAS part type_ (see Figure 6). 
This section of the tree is constructed dynamically - in other words, 
the subnodes present depend on the contents within - to prevent 
excessive scrolling.

![_The "Molecule contains PFAS parts larger than CF~2~/CF~3~" part of the OECD PFAS Definition node, with dynamic breakdown of subnodes by isolated PFAS part count (numbers from 25 Jan. 2025)._](fig/OECDPFAS_largerPFASparts.png)


#### The _Breakdown by isolated PFAS part count_ 
is first subset by the number of parts (Figure 5, left panel). 
Should there be fewer than ~20 categories, 
the immediate breakdown is by the formula of the parts (see
_e.g.,_ Figure 5, bottom right, "_Contains 15 isolated PFAS parts_").
Should there be more than 20 entries, an extra layer is added, 
to sort by the type of PFAS part (see Figure 5, top right, 
"_Contains 10 isolated PFAS parts_").
For categories with very large numbers of entries, an additional 
initial breakdown by the count of molecules is added (Figure 5, 
middle panel). This is again broken down dynamically. If only a
few subcategories exist, these are presented immediately thereafter 
(see Figure 5, bottom middle - several linear categories with many 
molecules). If, however, more breakdown is needed, an additional
set of part type nodes is added (_e.g.,_ Figure 5, middle panel, "_Count
of molecules 00001-10_") before the formula breakdown. 
Note that throughout the tree, leading zeros are present to 
ensure logical sorting. 


#### The _Breakdown by isolated PFAS part type_ 
is first broken down by the 
part type (linear, cyclic, _etc._) as shown in Figure 6, left panel. These are 
again split dynamically. With fewer than 20 entries, the list split
according to PFAS part formulas appears. If a greater breakdown is needed, 
an extra layer of "_Also contains ..._" or "_Only contains ..._" is 
added for extra navigation (_e.g.,_ Figure 6, mid left, "_Contains isolated 
branched PFAS part_"). For entries containing many CIDs, a breakdown 
by count of molecules is added (_e.g.,_ Figure 6, mid left, "_Contains isolated
linear PFAS part_"). Generally, the linear entries contain more entries 
than the other PFAS part types - and thus tend to have greater breakdown.
Some of these are broken down further (_e.g.,_ Figure 6, right), such that
a breakdown by the count of PFAS parts is added before the 
breakdown by "_Also contains..._".


![_The "Molecule contains PFAS parts larger than CF~2~/CF~3~" part of the OECD PFAS Definition node, with dynamic breakdown of subnodes by isolated PFAS part type (numbers from 25 Jan. 2025)._](fig/OECDPFAS_PFAS_part_type.png)


The dynamic navigation approach reduces the scrolling by users and 
also helps reduce the data loading time when many entries are 
present within a node. It is possible to use some 
advanced search and querying capabilities to improve the interaction
with the tree, see examples in [Exploring the Tree](#search) below. 

The _PFAS Parts Larger than CF~2~/CF~3~_ is available as 
a [MetFrag](https://msbi.ipb-halle.de/MetFrag/) 
[@ruttkies_metfrag_2016] file for further use 
[@pubchem_pfas_metfrag_2025]. The CSV can be downloaded from Zenodo
(DOI: [10.5281/zenodo.6385953](https://doi.org/10.5281/zenodo.6385953))
for use in 
[MetFragCL](https://ipb-halle.github.io/MetFrag/projects/metfragcl/) 
and is available from the 
[MetFragWeb](https://msbi.ipb-halle.de/MetFrag/) 
dropdown menu. This file contains several useful fields 
from the [Download](#pc-search) file as described further below. 
See the description on the Zenodo record 
[@pubchem_pfas_metfrag_2025] for more details. 



### Organofluorine Compounds {#orgf}

This node contains _Organofluorine compounds_ as defined in Figure 8 in 
the 2021 OECD PFAS Report 
[ENV/CBC/MONO(2021)25](https://one.oecd.org/document/ENV/CBC/MONO(2021)25/En/pdf) 
[@oecd_reconciling_2021]. 
Figure 7 (below) shows an extract from Figure 8 of 
the OECD report on the left panel, 
and the corresponding node breakdown in the _Organofluorine compounds_ 
section of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) 
to the right. Note that one additional 
category was added ("_Other fluorinated substances_") to capture content 
that did not fit into any other category defined in Figure 8 from the OECD Report. 

![_The categorization of PFAS (blue shading) and non-PFAS (grey) from the OECD 2021 report [@oecd_reconciling_2021] (left panel) and the "Organofluorine compounds" node (right panel). Numbers from 25 Jan. 2025._](fig/Organofluorine_OECDfig_tree.png)

The _Organofluorine compounds_ node is broken down very differently
to the _OECD PFAS Definition_ node, since not all the contents are 
PFAS (and thus do not contain PFAS parts). Each subnode is broken down first 
by the number of fluorine atoms (1 through to 15, then >15) and then by 
an exact mass range. If there are no CIDs for the given category, it is not
present. For instance, the "_Fluorinated aliphatic substances that have a 
fully fluorinated methyl or methylene carbon atom_" category starts at 
"_Contains 02 Fluorine atoms_" as no entries in this category could contain 
only one F. 
The exact mass subcategories are split into the ranges 
1-250, 250-500, 500-750, 750-1000 and >1000 - and are only present
if there are CIDs within this range.


### Other Diverse Fluorinated Compounds {#divf}

The "_Other Diverse Fluorinated Compounds_" section of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
is designed to help users explore various 
cases of fluorine chemistry that are not necessarily covered in the 
[OECD PFAS](#oecddef) 
or [Organofluorine compound](#orgf) sections above. The navigation in this 
section helps explore fluorinated compound chemistry by various
fluorine-heteroatom bonds and the occurrence of different elements
(see Figure 8). 

Many of the compounds present in this section are also present 
in the other sections of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120).
The overlap can be investigated in Entrez (see section 
[Interactions via Entrez](#entrez) below).

![_The "Other diverse fluorinated compounds" part of the PubChem PFAS Tree, showing the breakdown by fluorine bonded to non-carbon elements and by non-organic element. Numbers from 25 Jan. 2025._](fig/DiverseFcmpds.png)


#### The _Contains fluorine bond to non-carbon element_ 
section (Figure 8, middle panel) is broken down first by the 
count of molecules present in the given category, then by the 
non-carbon element present in the F-element bond (sorted alphabetically).
For the sections with counts above 100, there is an extra breakdown
by the numbers of fluorine present overall.


#### The _Contains non-organic element_ 
section (Figure 8, right panel) is likewise broken down first by the 
count of molecules present in the given category, then by the 
non-organic element present (sorted alphabetically).
In this section, non-organic refers to any element that is not 
C, H, N, O, P, S, Si, F, Cl, Br or I.
As above, there is an extra breakdown by the numbers of fluorine 
present overall for the sections with counts above 100.



### PFAS and Fluorinated Compound Collections {#lists}

The "_PFAS and Fluorinated Compound Collections_" 
section of the PubChem PFAS tree contains various lists gathered 
across PubChem content (see Figure 9).
The mapping files to construct this are kept 
on the [eci/pubchem](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/) 
repository on GitLab.
Currently, the content displayed in Figure 9 comes from:

- All [PFAS lists](https://comptox.epa.gov/dashboard/chemical-lists?filtered=&search=PFAS) 
from the 
[CompTox Chemicals Dashboard](https://comptox.epa.gov/dashboard/)
[@williams_comptox_2017] via the 
[EPA DSSTox Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=105) 
in PubChem;
- All [PFAS lists](https://zenodo.org/communities/norman-sle/search?page=1&size=20&q=PFAS) 
from the NORMAN Suspect List Exchange 
([NORMAN-SLE](https://www.norman-network.com/nds/SLE/)) 
[@mohammed_taha_norman_2022] via the 
[NORMAN-SLE Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=101) 
in PubChem;
- The CORE and Patent PFAS lists from OntoChem [@barnabas_extraction_2022];
- Other collections from within PubChem Classification Trees, including
collections from
[Cameo](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=86),
[ChEBI](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=2) and
[MeSH](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=1);
- The [NIST PFAS Suspect list](https://data.nist.gov/od/id/mds2-2387)
list provided by Benjamin Place [@benjamin_place_suspect_2021].

![_The "PFAS and Fluorinated Compound Collections" node, with all major collections shown (CompTox and OntoChem as insets). Numbers and content listing from 25 Jan. 2025._](fig/PFAS_list_of_lists.png)

Additional community-based PFAS can also be added to this section.
Ideas and suggestions for new lists are welcome and will be added 
if feasible and possible. Please email suggestions or ideas to 
[pubchem-help@ncbi.nlm.nih.gov](mailto:pubchem-help@ncbi.nlm.nih.gov) or 
[Emma Schymanski](mailto:emma.schymanski@uni.lu).


### Regulatory PFAS Collections {#regcoll}

Several regulatory PFAS collections from a variety of regulatory
documents are currently included in the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
as listed below, shown in Figures 10 & 11 and documented (as slides) in @schymanski_how_2022. 
Please note that this section of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
is currently in active 
development with the community. Please email suggestions or ideas to 
[pubchem-help@ncbi.nlm.nih.gov](mailto:pubchem-help@ncbi.nlm.nih.gov) or
[Emma Schymanski](mailto:emma.schymanski@uni.lu) directly. The use of 
[SMARTS](https://www.daylight.com/dayhtml/doc/theory/theory.smarts.html)
is explained in the tooltips. 

The regulatory PFAS collections currently include:

* Long-chain perfluorocarboxylic acids (LC-PFCAs) and related substances
  + C9-C21 LC-PFCAs as nominated for the Stockholm Convention
* Perfluorohexane sulfonic acid (PFHxS) and related substances
  + PFHxS and related compounds as defined in Annex A of the Stockholm Convention
  + PFHxS (linear or branched) plus its salts and related substances according to EU REACH (draft definition)
  + Difference between Annex A and EU REACH definitions
* Perfluorooctanoic acid (PFOA) and related substances
  + PFOA and related compounds as defined in Annex A of the Stockholm Convention
* PFOA and related substances - exclusions
* Perfluorooctane sulfonic acid (PFOS) and related substances
  + PFOS, PFOSF and related substances as defined in Annex B of the Stockholm Convention

![_The "Regulatory PFAS collections" part of the PubChem PFAS Tree, showing the major classes covered and a more detailed breakdown for PFOS (25 Jan. 2025)._](fig/RegCollectionOverview.png)

![_The "Regulatory PFAS collections" part of the PubChem PFAS Tree, showing a partial breakdown for the  PFHxS subsection, including annotation breakdown (25 Jan. 2025)._](fig/RegCollectionPFHxS.png)

As shown in the figure above, the regulatory collections also include 
detailed breakdowns of the contents according to annotation information 
present in the download files 
(described further in Section [Download via PubChem Search](#pc-search)).
A section including recent CIDs is also included in the major regulatory
definitions, allowing users to find relevant (by annotation) and recent
(by date) entries. Categories follow the major headings of the 
[PubChem Table of Contents](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72)
and are patents, literature, use, safety and toxicity information.


### PFAS Breakdowns by Chemistry {#chem}

The [PFAS breakdowns by chemisty](#chem) section is an expansion 
of the [OECD PFAS definition](#oecddef) that also includes salts and 
mixtures, not just neutral compounds. This section contains four
major breakdowns, by _composition_ (neutral vs. salt/mixture),
by _functional groups_, by _connectivity degree_ (PFAS part connected
to one or more non-PFAS parts) and by _PFAS part formulas_ (i.e., 
length of the PFAS), as shown in Figures 12 and 13.  

![_The "PFAS breakdowns by chemistry" part of the PubChem PFAS Tree, showing the four major nodes, the first sublayer of each and some subsections. Numbers from 25 Jan. 2025._](fig/PFAS_breakdown.png)


![_Substructure of the "Breakdown by PFAS composition", "Breakdown by PFAS part connectivity degree" and "Breakdown by PFAS functional groups" sections. Numbers from 25 Jan. 2025._](fig/PFAS_breakdown_comp_conn.png)

This section can be used to explore many functional properties about
PFAS compounds, more examples will be shown in the following sections.

## Exploring the PubChem PFAS Tree {#search}

While the tree offers several possibilities for browsing and searching
PFAS and other organofluorine content, there are more powerful
search capabilities to empower this further, as explained in the next 
sections.

### Download via PubChem Search {#pc-search}

Perhaps the most intuitive interaction is directly through 
clicking on the numbers besides each node (see Figure 14). This sends a query 
directly to the PubChem Search interface and displays the 
entire node contents, as shown in Figure 14. This query follows 
"_OECD PFAS definition_" > "_Molecule contains PFAS parts larger than 
CF~2~/CF~3~_" > "_Breakdown by isolated PFAS part count_" > 
"_Contains 01 isolated PFAS part_" > "_Count of molecules 10001-100000_" >
"_Contains 01xC04F09-linear_" and returns 12,277 CIDs (25 Jan. 2025) 
containing only one single linear C~4~F~9~ PFAS part. 
This query can then be downloaded or saved (Figure 14, insets), 
or sent to Entrez for advanced querying (see [section on Entrez](#entrez)).
Note that clicking on the "**?**" beside a node (where present) will open a 
tool tip explaining the node contents (Figure 14, grey shading).

![_Querying node contents in PubChem Search. When clicking on the blue numbers (left), a search window will open in a new tab (right, main image). This collection can be browsed, downloaded or saved (see insets) or sent to Entrez (see next section). Clicking on the "**?**" sign next to a node name will open a tool tip (left panel, grey shading). Figure updated 25 Jan. 2025._](fig/Tree_PubChemSearch.png)

The download file contains a number of fields of interest, highlighted in 
Figure 15, 
including: PubChem compound identifier (CID), names and synonyms, 
several properties (_e.g._ XlogP, molecular formula, masses), 
structural information (SMILES, InChI, InChIKey), patent and literature counts
as well as several metadata entries. These metadata entries contain
valuable information about the evidence contributing to the presence
of that structure in PubChem (_e.g.,_ contribution source(s) and date, 
annotation information). Relevant fields are explained in Table 2
and shown in Figure 15. 

Note that the categories visible in the "_annothits_" column align 
with the individual sections in PubChem records and can 
also be viewed in the 
[PubChem Table of Contents (TOC) Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72).
For any entry with annotation, the information 
available can be viewed for that individual CID.
For example, the annotation information for 
CID [67814](https://pubchem.ncbi.nlm.nih.gov/compound/67814) 
in Figure 15 can be viewed for the following sections (selected examples):
[Classification](https://pubchem.ncbi.nlm.nih.gov/compound/67814#section=Classification), 
[Names and Identifiers](https://pubchem.ncbi.nlm.nih.gov/compound/67814#section=Names-and-Identifiers), 
[Patents](https://pubchem.ncbi.nlm.nih.gov/compound/67814#section=Patents), 
[Safety and Hazards](https://pubchem.ncbi.nlm.nih.gov/compound/67814#section=Safety-and-Hazards), 
[Use and Manufacturing](https://pubchem.ncbi.nlm.nih.gov/compound/67814#section=Use-and-Manufacturing).


![_PubChem Download file. Top left: PubChem Compound Identifier (CID), names, properties. Middle: structural information, names, more properties. Bottom: more properties, patent and literature counts, annotation content, CID dates. Downloaded from the query shown in Figure 14 on 20 June 2023._](fig/PubChem_Download_File.png)


Table: _Relevant metadata files in the PubChem Download files._

| Header | Description | Type |
|-----------|---------|----|
|annothits| Annotation categories present for this CID | Text |
|annothitcount | Count of annotation categories for CID | Numeric |
|cidcdate | CID creation date | YYYYMMDD |
|depcatg | Deposition category, reveals what type of sources | Text |
|         |  contributed information   |   |
|pclidcnt | Consolidated literature count | Numeric |
|gpidcnt | Patent count | Numeric |
|sidsrcname | Name of the data source(s) contributing substance | Text |
|         |  (SID) information for given CID  |   |


There are many records where the information 
has only been extracted from patents, or for which no annotation exists. 
Thus, the various metadata fields listed in Table 2 can help add a lot 
of context to the relevance of the 
entries for the particular question at hand. 
More advanced queries are possible to leverage this information even further, 
as explained in the next sections. 

### PubChem "Saved Searches" {#savedsearch}

The PubChem "Saved Searches" feature can be used to save and interact 
with different searches using the Boolean operators "AND", "OR" and "NOT". 
Any section of any classification browser can be sent to PubChem Search
(or uploaded via the "Upload ID list" option on the landing page). 
For instance, the saved searches shown in Figure 16 can be used to 
find out how many Agrochemicals are OECD PFAS with data in MassBank.EU.
The window shown in Figure 16 was created by saving the 
"PFAS breakdowns by chemistry" section of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
as "OECD PFAS (incl salt/mix)", then saving the 
"Agrochemical Information" section of the 
[PubChem Compound TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72)
as "Agrochemicals", then saving the "Information Sources > MassBank Europe"
section of the 
[PubChem Compound TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72)
as "MassBank Europe", then using the "AND" functionality at the top 
to build the respective queries. 

\newpage

These results can be viewed again 
in the PubChem Search interface (shown in Figure 14) and sent to Entrez 
(explained in next section) to see _e.g._, the breakdown of Agrochemicals
according to various categories of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
as shown to the right of Figure 16.


![_Left: "Saved Searches" in PubChem, with query builder (top). Right: Viewing the results opens a PubChem Search window; the results can be sent to Entrez (see Figure 14), selected from a dropdown menu and browsed in the PubChem PFAS Tree (a refresh may be necessary). Updated 25 Jan. 2025._](fig/SavedSearches.png)


### Interactions via Entrez {#entrez}

It is possible to build more extensive queries via the
[Entrez](https://pubchem.ncbi.nlm.nih.gov/docs/advanced-search-entrez)
interface, which is accessible through the button below
the download button (see Figure 14) or by clicking the "Use Entrez"
option on the PubChem landing page. More documentation on Entrez is given
[here](https://pubchem.ncbi.nlm.nih.gov/docs/advanced-search-entrez). 
It is also possible to send queries to Entrez via the
PubChem Identifier Exchange Service
([ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi)),
as shown in Figure 17.

![_Sending queries to Entrez via the [PubChem ID Exchange](https://pubchem.ncbi.nlm.nih.gov/idexchange/idexchange.cgi)._](fig/IDExch_to_Entrez.png)


This rest of this section steps through a few interactive examples.

#### Example 1: Find all PFAS containing one linear C~4~F~9~ part with use information:
To find all molecules from the query in Figure 14 that also have
use information in PubChem, the first step is to send the 12,277 CIDs
from the query above to Entrez via the "Push to Entrez" option (Figure 14,
second box encircled in red on the right). This opens a new page in the
Entrez interface (not shown).
Next, go to the "Use and Manufacturing" section of the
[PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72),
send this to PubChem Search via the numbers next to the node (Figure 18,
red circle on left), and push to Entrez (Figure 19, top right). By
selecting the "Advanced" option under the search bar (Figure 18, top),
the Advanced Search builder is opened and further queries can be built.
By selecting "#5 AND #8", only the 431 chemicals with a single
C~4~F~9~ linear PFAS part (query #5) that also have use and manufacturing
information in PubChem (query #8) are returned.

![_Advanced search via Entrez. Left: [PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72). Top right: the Use and Manufacturing query in Entrez. Bottom right: the Advanced Search builder in Entrez, where query #5 (one C~4~F~9~ part only) AND #8 (Use information) is built. This is then sent again to search via Entrez (middle right) and the 431 C~4~F~9~ compounds with use information can be browsed or downloaded via the "View or Download Structures in PubChem" option. Queries run on 25 Jan. 2025._](fig/Entrez_C4F9andUse.png)


#### Example 2: Browse all OECD PFAS with mass spectrometry information:
The Entrez functionality can be used to find out 
which PFAS or organofluorine compounds have mass spectrometry information
available in PubChem (or in resources integrated within PubChem). 
The tree contents can be subset according to other available information, as 
shown in Figure 19. First, go to the "Mass Spectrometry" section of the
[PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72),
under the "Spectral Information" heading, and send this query
to Entrez (see Figure 19 left and top right). Then, go back to the
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120)
and ***refresh*** the contents. A new dropdown menu will appear
(if not already present) called "Filter by Entrez History" (Figure 19,
bottom right). By selecting the chosen query in this dropdown menu,
the tree will then be subset by the contents within that query, such
that only CIDs that are in the tree _and_ in the query will show
(in Figure 19, ~58K not 22M CIDs).
The same holds for any advanced query, so it would be possible to
_e.g._ do a subset of only mass spectra that occur in
[MassBank EU](https://massbank.eu/MassBank/) or NIST by additionally
adding the relevant "Information Sources" (from the
[PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72))
to the Entrez query. Since large queries such as the "Mass Spectrometry"
category, or advanced AND/OR combinations can end up quite complicated,
noting the query number (#XXX) and the number of compounds in the result
can be helpful. Alternatively, use the Saved Searches functionality to name 
the searches before sending them to Entrez (Figure 16).

![_Subsetting Tree Contents via Entrez. Left: [PubChem TOC Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=72), "Mass Spectrometry" subsection. Top right: the "Mass Spectrometry" query in PubChem Search (to be sent to Entrez). Bottom right: the [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) subset by Mass Spectrometry, now only displaying CIDs where mass spectrometry information is available in PubChem. Queries run on 25 Jan. 2025._](fig/Entrez_MSandPFAS.png)

![_Subsetting Tree Contents via Entrez. Left: [Aggregated CCS Classification Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=106). Right: the [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) subset by CCS values, now only displaying CIDs where collision cross section information is available in PubChem. Queries run on 25 Jan. 2025._](fig/CCStoPFAStree.png)


#### Example 3: Browse all PFAS with CCS information:
Figure 20 (previous page) shows how to explore the PFAS
with collision cross section (CCS) values using the 
[Aggregated CCS](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=106)
classification. 

<!-- ![_Subsetting Tree Contents via Entrez. Left: [Aggregated CCS](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=106). Right: the [PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) subset by CCS values, now only displaying CIDs where collision cross section information is available in PubChem. Queries run on 21 June 2023._](fig/CCStoPFAStree.png) -->

## Extra Details {#details}

This documentation is primarily aimed at describing the features of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120). 
<!-- with the full implementation to be described elsewhere (coming soon).  -->
This section includes some additional technical details, which
will be expanded as further questions arise.

### Programmatic Interactions via PUG REST {#pugrest}

It is possible to interact with the PubChem PFAS Tree 
programmatically. For more extensive details on PUG REST 
and other programmatic access than contained below,
please see the following locations in the PubChem documentation: 

 - https://pubchem.ncbi.nlm.nih.gov/docs/programmatic-access
 - https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest
 - https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest-tutorial
 - https://pubchem.ncbi.nlm.nih.gov/docs/pug-rest#section=Classification-Nodes 

Example code describing how to interact with the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) is provided in a separate document, available as
[PFAS_Tree_in_R.pdf](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/blob/main/pfas-tree/PFAS_Tree_in_R.pdf) or 
[PFAS_Tree_in_R.Rmd](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem-docs/-/blob/main/pfas-tree/PFAS_Tree_in_R.Rmd).


### Areas of Development
There are currently several areas of potential future development, including: 

- Handling of ethers and other connecting atoms;
- Adding salts/mixtures into the organofluorine and other fluorinated content sections
- Handling of polymer and poorly defined entries.

#### Polymers/Poorly defined entries: 
Since the entire 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) 
is constructed on CIDs (_i.e._, compounds), substance entries 
(denoted by substance identifiers, SID) are not included. Thus, 
undefined or poorly defined entities and polymers are not included
(such as the example in Figure 21). 
More information about the difference between compound and substances 
on PubChem is available
[here](https://pubchem.ncbi.nlm.nih.gov/docs/compound-vs-substance).

![_An example of a polymer not yet included in the PFAS Tree - [Teflon](https://pubchem.ncbi.nlm.nih.gov/compound/Polytetrafluoroethylene)._](fig/Polymer_example.png)


### PFAS Test set
A test set of PFAS and non-PFAS from the OECD Report 
[@oecd_reconciling_2021] has been compiled to check the 
performance of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120).
The test set (XLSX) can be downloaded 
[here](https://gitlab.com/uniluxembourg/lcsb/eci/pubchem/-/raw/master/annotations/pfas/OECD_Report_Examples.xlsx?inline=false). Other formats can be made available if 
requested (and if reasonably possible).

### Downloading large files
Attempting to download nodes containing millions of entries can result 
in download files that exceed Microsoft Office size limits. Adjusting 
[this example download URL](https://pubchem.ncbi.nlm.nih.gov/sdq/sdqagent.cgi?infmt=json&outfmt=csv&query={"download":"cid,inchikey,cmpdsynonym","collection":"compound","where":{"ands":[{"input":{"type":"netcachekey","idtype":"cid","key":"NnGVh8aFozmUFysOqXZiJEtfhz_aVSglUgAzaUkRIWhJCB0"}}]},"order":["relevancescore,desc"],"start":1,"limit":10000000,"downloadfilename":"PubChem_compound_list_NnGVh8aFozmUFysOqXZiJEtfhz_aVSglUgAzaUkRIWhJCB0"})
can be used to select columns and row numbers, to navigate around the limits. 
Please note that the cache key will have to be replaced by an active
download query cache key for this URL to work. 

## Contact Details

User feedback is extremely valuable to help improve this tree further. 
Suggestions for PFAS or fluorinated compound collections to 
include in the "_PFAS and Fluorinated Compound Collections_" section of the 
[PubChem PFAS Tree](https://pubchem.ncbi.nlm.nih.gov/classification/#hid=120) 
are also welcome. 
Feedback or suggestions can be sent to 
[Emma Schymanski](mailto:emma.schymanski@uni.lu) and/or
[pubchem-help@ncbi.nlm.nih.gov](mailto:pubchem-help@ncbi.nlm.nih.gov).


For general questions about PubChem and the functionality
described here, please reach out to the 
[PubChem Help mailing list](mailto:pubchem-help@ncbi.nlm.nih.gov)
for further support. 



## Statements {#statements}

### Author Contributions {#contrib}
<!-- https://authorservices.wiley.com/author-resources/Journal-Authors/open-access/credit.html -->

ELS: Conceptualization (equal), data curation, methodology, software,
validation, writing - original draft preparation, writing - review and editing, writing - updates.
PC: Validation (supporting). TK: Software. 
PAT: Data curation, methodology, software.
JZ: Data curation, methodology, software.
EEB: Conceptualization (equal), data curation, methodology, software (lead),
validation, writing - original draft preparation, writing - review and editing.


### Acknowledgements {#ack}

We would like to acknowledge discussions with Zhanyun Wang (EMPA, CH), 
Hans Peter Arp (NGI, NO), Ian Cousins, Luc Miaz, Jon Martin (ACES, SE),
as well as other project members of [ZeroPM](https://zeropm.eu/). We 
acknowledge many valuable discussions with Andreas Buser (FOEN, Switzerland)
during development of the Regulatory collections [@schymanski_how_2022]. 
We would also like to acknowledge discussions and contributions from 
various members of the ECI and PubChem teams that were not directly
involved in these efforts but have contributed indirectly 
through our many other collaborative efforts!

## References {#refs}

